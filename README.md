# コーディング規約まとめ
------------

## はじめに
各規約はMarkDown形式で記述しています。
ローカル環境で閲覧する場合はビューアでの閲覧を推奨します。
ビューアの指定は特にありませんが、以下で閲覧できることは確認しました。

* [Markdown Viewer :: Add-ons for Firefox](https://addons.mozilla.org/ja/firefox/addon/markdown-viewer/)


## Ruby

* [Ruby](master/Ruby/ruby.md)
* [Ruby on Rails](master/Ruby/rails.md)
* [RSpec](master/Ruby/rspec.md)
* [モック作成](master/Ruby/mock.md)
* [html/css](master/Ruby/html-css.md)