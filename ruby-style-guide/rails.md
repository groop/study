# Ruby on Railsのコーディング規約
----------

## はじめに
----------

### 対象バージョン
3.2系を対象とします。


### Rubyについて
Rubyのコーディング規約を前提に進めます。

[Rubyのコーディング規約](ruby.md)


## 命名規約
----------
### 共通
名称は可能な限り省略を行わない。

### コントローラ・アクション
コントローラ名は操作対象を簡潔に表す名称をつける。

アクション名はScaffoldingの名称をベースに以下の様なルールをベースに名付ける。

|役割      | アクション名  |
|-------- | --------- |
|一覧      | index     |
|新規登録入力   | new       |
|新規登録確認   | new_confirm  |
|新規登録実行   | create    |
|詳細      | show      |
|編集入力      | edit      |
|編集確認      | edit_confirm |
|編集実行      | update      |
|削除実行      | destroy      |



### モデル名(テーブル名)
モデルは基本的に単数形で定義し、テーブルの実態は複数形になるようにする。

	# モデルの生成
	rails g model user username:string

これにより、テーブル名はusersとして定義される。


### カラム名
カラム名は全テーブルで以下のカラムを必須とする。

|物理名      | 役割  |
|--------   | --------- |
|id         | 主キー     |
|created_at | 登録日時   |
|updated_at | 更新日時   |

ただし、リレーションのみを提供する結合テーブルではこの限りではありません。

また、論理削除をサポートする場合、以下のカラムを追加して下さい。

|物理名      | 役割  |
|--------   | --------- |
|deleted_at | 削除日時   |

これは rails3_acts_as_paranoid による論理削除で使用されます。



外部キーは ** モデル名(単数形)_id ** として定義します。例えば、以下のように定義した場合、** user_id **が外部キー名となります。

	# ユーザを参照するモデルの作成
	rails g scaffold roles user:references name:string




## アプリケーション設定
----------
アプリケーションの設定は以下の場所に行う

* config/application.rb
:	アプリケーション共通の設定
* config/environments/development.rb
:	開発環境のみの設定
* config/enviroments/test.rb
:	テスト環境のみの設定
* config/enviroments/produnction.rb
:	本番環境のみの設定


## ルーティング
----------
RESTfulを念頭に定義して下さい。旧デフォルトである下記の形式は使用しないで下さい。

	# NG
	match ':controller(/:action(/:id))(.:format)'



## コントローラ
----------
コントローラに関わるgemとしては以下を導入しています。

* [Strong Parameters](https://github.com/rails/strong_parameters)
:	許容しないパラメータをModelに渡さないようにする



## ビュー
----------
ビューのテンプレートはslimを利用しています。slimについては[Mock作成ガイドライン](mock.md) も参照して下さい


#### フォームビルダー
TwitterBootstrapに対応するため、** application_form_for ** を定義してあります。標準のform_forではなく、そちらを利用して下さい。

また、原則として、新たにビルダーを生成することは禁止します。


#### ビューヘルパー
アプリケーション共通のヘルパーは ** app/helpers/application_helper.rb **に定義してください。

局所的なヘルパーは ** app/helpers/コントローラ名_helper.rb ** として定義して下さい。

ただし、** app/helpers/ ** 以下に定義したものは、すべてのページで有効となってしまいます。
衝突を避けるためにも、メソッド名にはコントローラ名を先頭につけることを推奨します。



## モデル
----------

#### DBMSに依存した記述について
使用するDBは以下のように定義する。

* 開発環境
:	SQLite
* テスト環境
:	MySQL
* 本番環境
:	Oracle

この為、DBMSに依存した書式を極力排除する必要がある。
具体的には、以下のメソッドに対して利用の制限を設ける。

* find_by_sql

また、whereやorderなどで依存する記述が必要になる場合、モデルにラッピングメソッドを用意する。


##### 参考
* [DBの種類に依存せずにcreated_atのdateによるgroup byをするには？ - QA@IT](http://qa.atmarkit.co.jp/q/2827)


#### プレースホルダ


パラメータ数が3以下の場合は**名前なしパラメータ**を、それ以上の場合は**名前付きパラメータ**を使用する。

パラメータ名はカラム名と同名のものを使用する。それ以外の場合は簡潔にわかりやすい名前をつける。


#### データ検索
一般的な検索は ransack を利用すること。

* [Ransackのススメ #Rails #ransack #Ruby - Qiita [キータ]](http://qiita.com/items/9a95d91f2b97a08b96b0)
* [Basic Searching · ernie/ransack Wiki](https://github.com/ernie/ransack/wiki/Basic-Searching)



## 定数
----------


## マイグレーション
----------


## Assets
----------



## CoffeeScript
----------

* [polarmobile/coffeescript-style-guide · GitHub](https://github.com/polarmobile/coffeescript-style-guide)


## Bundler
----------
最低限、開発・テストと本番のグループ分けは行う。

* development
:	開発環境で利用
* test
:	テスト環境で利用


## Action Mailer
----------


## PDF出力
----------
thinreportsを利用しています。

* [ThinReports](http://www.thinreports.org/)


#### フォーマットの作成
エディタをインストールして作成。

* [Download | ThinReports - 帳票ソリューション for Ruby and Rails](http://www.thinreports.org/download/)




## エクセル(xlsx)の出力
----------

* [axlsx](https://github.com/randym/axlsx)



## 多言語対応
----------
言語ファイルは ** RAILS_ROOT/config/locales/ ** に入っています。多言語化しないサービスの場合でも、カラムの物理名や、自動出力の文言の変更などに利用されます。

* [i18n_generators](https://github.com/amatsuda/i18n_generators)


## 初期データ
----------
初期データの定義として以下のパターンがあります。

* マスタデータ
:	環境に関わらず必ず利用するデータ
* 開発時のデータ
:	開発時にのみ参照するダミーデータ
* テスト時のデータ
:	各種自動テストを行う際に利用するデータ

これらはそれぞれ以下の場所で定義します。


#### マスタデータ
** db/seeds.rb ** にて登録ロジックを記載します。マスタデータの更新が必要になる場合にもここにロジックを記載し、新規・更新どちらにも対応するようにしてください。


#### 開発時のデータ
** spec/features ** 以下に各テーブルのデータを定義して下さい。


#### テスト時のデータ
factory_girl を利用します。 ** spec/factories ** 以下に定義して下さい。

* [factory_girl](https://github.com/thoughtbot/factory_girl_rails)

ここで定義したものは自動登録されません。テストコードの方で必要なデータを参照して下さい。



## デバッグ
----------
幾つかデバッグに役立つgemを入れています。

* [better_errors](http://morizyun.github.io/blog/better-error-gem-rails-ruby-rack/)
:	例外エラーページを見やすく
* [debugger](https://gist.github.com/tkyowa/1378694)
:	デバッガによるステップ実行等


一番単純なデバッグ方法としては、任意の場所で ** raise ** で例外を発生させて、** p ** で値の確認を行うことです。

経過を見て行きたい場合は、debuggerによるデバッグコンソールを利用して下さい。



## ドキュメント
----------
コメントはRubyの規約をベースにyard形式で記述してください。

* [Rails/Rubyドキュメントをキレイに生成するYARD、早見表付き！ - 酒と泪とRubyとRailsと](http://morizyun.github.io/blog/yard-rails-ruby-gem-document-html/)

#### ドキュメントの閲覧
yardを起動してください。

	# yard serverの起動
	bundle exec yard server --reload

このあと、 [http://localhost:8808/](http://localhost:8808/) にアクセスすれば見られます。

また、gemのドキュメントを見たい場合は以下のコマンドを実行して下さい。

	# yard serverの起動
	bundle exec yard server --gems


##### ドキュメントの生成
静的ファイルに吐き出したい場合は以下のコマンドを実行して下さい。

	bundle exec yard doc

これにより、doc以下にドキュメントが生成されます。


## CoffeeScriptのドキュメント
----------
CoffeeScriptのドキュメントはcodoを利用します。初期期はyardと似ています。

* [netzpirat/codo](https://github.com/netzpirat/codo)


#### codoのインストール
node.jsがインストールされていればnpmコマンドが通るはずです。まずはインストールされていることを確認して下さい。

	# npmの確認
	npm -v

バージョンが表示されていればokです。表示されない場合は、node.jsのインストール手順を再確認して下さい。

	# codoのインストール
	npm install -g codo


#### ドキュメントの閲覧
yardとは違い、ドキュメントの動的な生成は行なってくれません。

	# ドキュメントの生成
	rm -rf .codoc
	codo
	
	# ドキュメントサーバの起動
	codo -s



## テスト
----------
テストについては [RSpec](rspec.md) を参照して下さい。


## 本番環境での動作
-----------
実際には ** unicorn **などで動かすはずなので、この通りにはならないが、とりあえず本番モードでの動作。

	# assetsのプリコンパイル
	bundle exec rake assets:precompile RAILS_ENV=production

	# 本番環境のDB構築
	rake setup RAILS_ENV=production
	
	# rails 起動
	rails s -e production


* [Rails3.2で production にした途端にapplication.css が参照できなくなる:お題目うぉっち](http://blog.livedoor.jp/maru_tak/archives/51247889.html)


## その他
-----------


### 参考
* [コーディング規約をまとめてみた (Rails編) - bojovs::blog](http://bojovs.com/2012/10/16/rails-coding-style)