# Rails + RSpecによる各種テスト
----------

## はじめに
----------
テストの種類は大きく分けて以下のようになります。

* Controller
* View
* Model
* Helper
* Routing
* Rakeタスク
* その他クラス
* 総合テスト

基本的にすべてRSpecによる記述を行いますが、
総合テストだけはRSpec + Capybara + Turnipという構成になります。


## テストを始める前に
----------

#### テストデータの作成

テストデータの作成はFacrotyGirlを利用します。


[[rails]has_manyなフィクスチャを書くのに疲れたらFactory Girlがオススメ！ - func09](http://www.func09.com/wordpress/archives/532)



#### テスト用のヘルパー作成

** 作成中 **


#### 共通事項
基本的に、テスト対象は**すべての公開メソッド**とします。
ただし、ViewやRakeタスクなどに例外もありますが、それは各項目で記載します。

commit または push を行う際には、必ず追加変更した動作に対応するテストを記載してください。
すぐに内容を記述できない場合でも、pending(保留)として手続きだけは必ず定義をしてください。
ただし、総合テストはプロジェクトによって記載タイミングを決定して下さい。


#### RSpecの基本

* [RSpecによるユニットテストの書き方 - tech.recompile.net](http://tech.recompile.net/post/21340599029/rspec)
* [RSpecの標準Matcher一覧表 - 本当は怖い情報科学](http://d.hatena.ne.jp/keisukefukuda/20080124/p1)
* [Rspec Rails cheatsheet (include capybara matchers)](https://gist.github.com/them0nk/2166525)


#### Windowsでの注意点

Windowsの場合、出力に色が反映されないので、以下のサイトを参考にansiconをインストールして下さい。

レジストリを使用しない場合ではbundlerやrakeが実行できないので、必ずレジストリに登録する方で行なって下さい。

* [Windows環境でRSpecなどの出力に色を付けるには - 萬由無事覚書](http://lajvard.hatenablog.com/entry/2012/03/03/194359)



## 各種コマンド
----------

#### 単体テストの実行

以下のコマンドで単体テストが実行できる。

	# 個別に実行
	bundle exec rspec "テストファイル名"

	# 全テスト実行
	bundle exec rake spec

テストの結果はその場で表示されるはずですが、htmlでも出力されるので
そちらも参照してみてください。

	# テスト結果のhtml
	open ./tmp/features.html


#### 総合テストの実行

以下のコマンドで総合テストが実行できる。

	# 個別に実行
	bundle exec rspec "テストファイル名(featureファイル)"

	# 全テスト実行
	bundle exec rake spec:features

テストの結果はその場で表示されるはずですが、htmlでも出力されるので
そちらも参照してみてください。

	# テスト結果のhtml
	open ./tmp/report.html

ただし、sporkを通したのテストの場合、エラー時のスクリーンショットが
取得出来ないことに注意して下さい。
これについては、今後対策を行うかもしれません。	


#### sporkの起動

連続的にテストを行う場合、sporkを起動しておいたほうが早いです。
簡単に説明すると、テスト毎にrailsを起動するのは時間が掛かるから、
最初から起動しておいて使いまわそうっていう仕組み。

ただし、Windowsの場合はsporkのスロットが二つしか用意されず、交互に実行されるため、
しばらく待たないとspork経由で実行できないことに注意が必要。

	# sporkの起動
	bundle exec spork

起動後に、前述のテスト実行などを行えば良い。configとかを書き換えつつのテストの場合、
自動で再読込されないことがあるようなので、その場合はsporkの再起動が必要になります。


#### 自動テスト(Guard)
ファイルの変更毎に自動的にテストを実行出来るように **Guard** を入れてあります。
これにより、Guardがソースコードの変更を監視し、変更があったソースに関連するテストを
自動で実行するようになります。

	# guardの起動
	bundle exec guard

macで*Growl*ユーザであれば、**GrowlNotify**をインストールすれば、テスト毎にGrowlによる通知が行われます。

前述のsporkを自動的に起動してテストが行われるので、sporkは別途起動する必要はありません。
Windowsの場合、前述のsporkの初期化処理の関係で、1テスト終了後、30秒くらい待つ必要があると思います。


#### カバレッジ(網羅率)について
COVERAGE=onにすれば、テストのカバレッジ(網羅率)を検出することもできます。
ただし、カバレッジを見たい場合、spork経由でのテストでは正常なカバレッジは取得できないので
sporkとguardは停止してから下記のコマンドを実行して下さい。	

	# テストの実行
	COVERAGE=on bundle exec rake spec

	# 出力結果表示
	open ./coverage/index.html

基本的には、担当箇所のカバレッジが100%になった状態で完了とします。


#### 総合テストの定義済みステップの一覧
定義済みの全ステップを記載したhtmlを出力することが可能です。
総合テストで共通ステップが多くなった場合などに使用して下さい。

	# table_beetの実行
	bundle exec table_beet --path=./spec/steps/

	# 出力結果の表示
	open ./stepdoc/index.html


## Controller
----------
* テスト対象
: `./app/controllers/**/*_controller.rb`
* テスト項目
: `./spec/controllers/**/*_controller_spec.rb`

* Controllerに関する単体テストを記述する
* テスト対象はすべてのコントローラの公開メソッドとする
* ログイン必須なページヘのテストに注意
* ビューしか存在しないアクションのテストは必要ない(そもそもアクションを記述しない)



## View
----------
* テスト対象
: `./app/views/**/*.*.*.rb`
* テスト項目
: `./spec/views/**/*.*.*_spec.rb`

* Viewに関する単体テストを記述する。
* テスト対象はすべてビューとする(原則としてPDFやxls出力も含まれる)
* **render**で呼び出すビューファイルに対する個別のテストは必要ない。
	* ただし、呼び出し元のテストファイルに記述すること。
* レイアウトビューに対するテストは・・・・


## Model
----------
* テスト対象
: `./app/models/**/*.rb`
* テスト項目
: `./spec/models/**/*_spec.rb`

* Viewに対する単体テストを記述する
* テスト対象はすべてのモデルの公開メソッドとする
* moduleでロジックを外に出している場合・・・
* ・・・・


**参考**

* [RspecによるRailsのモジュールのテスト（暫定案） 俺の備忘録](http://o.inchiki.jp/obbr/170)

## Helper
----------
* テスト対象
: `./app/helper/**/*.rb`
* テスト項目
: `./spec/helper/**/*_spec.rb`

* Helperに対する単体テストを記述する
* テスト対象はすべてのヘルパーの公開メソッドとする。


## Routing
----------
* テスト対象
: `./app/config/routes.rb`
	`./app/controllers/**/*_controller.rb`
* テスト項目
: `./spec/routing/**/*_spec.rb`

* アプリケーション内のルーティングに対する単体テストを記述する
* テスト対象はすべてのアクションに通じるルーティングとする


## Rakeタスク
----------
* テスト対象
: `./lib/tasks/**/*.rb`
* テスト項目
: `./spec/lib/tasks/**/*_rake_spec.rb`

* rakeタスクに対する単体テストを記述する
* テスト対象はすべてのタスクとする


## その他クラス
----------
* テスト対象
: `./lib/**/*.rb`
* テスト項目
: `./spec/lib/**/*_spec.rb`

* lib以下に配置したその他クラス類に対する単体テストを記述する
* テスト対象はすべての公開メソッドとする。



## 総合テスト
----------
* ステップ
: `./spec/steps/**/*_steps.rb`
* シナリオ
: `./spec/features/**/*.feature`

* 総合テストのステップとシナリオを記述する
	* ステップ = テストに使用するステップ(手順)を記述
	* シナリオ = 定義したステップを使ってシナリオを記述
* ログインなどの共通ステップはタグ(名前空間)無しで定義する
	* 各シナリオの個別ステップはすべてファイルパスに応じたタグを定義する



**参考**

* [jonleighton/poltergeist · GitHub](https://github.com/jonleighton/poltergeist)

