# HTML CSSガイドライン
----------

## はじめに
----------

### 対象バージョン
1.9.3系を対象とします。


## ソースコードの整形
----------

### インデント
インデントには半角スペースを使用し、幅は2とします。

### 大文字/小文字
小文字のみ使用する。alt属性など値が文字列の場合は除く。

	<!-- NG -->
	<A HREF="/">Home</A>

	<!-- OK -->
	<img src="google.png" alt="Google">

### 文末のスペース
文末のスペースを削除する。

	<!-- NG -->
	<p>What?_

	<!-- OK -->
	<p>Yes please.

### エンコード

	エンコードは、UTF-8（BOM無し）を使う。
	以下をHTMLファイルに記述してエンコードを指定。

	<meta charset="utf-8">

### コメント
必要に応じてコードの説明を記述する。全部に書けというわけではない。

### TODOコメント


## HTMLのスタイルルール
----------

### ドキュメントタイプ
HTML5を使うこと。以下で始まる形式で書く。XHTML5はNG。

	<!DOCTYPE html>


### HTMLのバリデート
可能な限り適切なHTMLを記述すること。
そうでないとパフォーマンスが低下するような場合でない限りは、ちゃんと書く。
「[W3C HTML validator](http://validator.w3.org/nu/)」などの検証ツールを使用する。


### セマンティックに書く
目的に応じてHTMLを記述する。
見出しならhx要素、段落ならp要素、アンカーならa要素など目的に応じたHTML要素を使う（「HTMLタグ」という言い方は間違い）。
リンクならa要素で書く。onclickのようなJavaScriptな振る舞いのものを要素の属性に入れない。

	<!-- NG -->
	<div onclick="goToRecommendations();">All recommendations</div>

	<!-- OK -->
	<a href="recommendations/">All recommendations</a>

### マルチメディアの代替コンテンツ
マルチメディアの要素には、代替コンテンツを提供する。
画像には、意味のある代替テキストをalt属性として、動画・オーディオコンテンツにはキャプションを記述する。
装飾的な用途の場合など意味を持たない画像については、代替テキストは記述せずにalt=”"とする。

	<!-- NG -->
	<img src="spreadsheet.png">

	<!-- OK -->
	<img src="spreadsheet.png" alt="Spreadsheet screenshot.">


### 構成要素の分離
文書構造・見た目・振る舞いは、分離すること。
見た目に関するものはスタイルシートに、振る舞いに関するものはスクリプトへ移して記述する。

	<!-- NG -->
	<!DOCTYPE html>
	<title>HTML sucks</title>
	<link rel="stylesheet" href="base.css" media="screen">
	<link rel="stylesheet" href="grid.css" media="screen">
	<link rel="stylesheet" href="print.css" media="print">
	<h1 style="font-size: 1em;">HTML sucks</h1>
	<p>I’ve read about this on a few sites but now I’m sure:
	  <u>HTML is stupid!!1</u>
	<center>I can’t believe there’s no way to control the styling of
	  my website without doing everything all over again!</center>

	<!-- OK -->
	<!DOCTYPE html>
	<title>My first CSS-only redesign</title>
	<link rel="stylesheet" href="default.css">
	<h1>My first CSS-only redesign</h1>
	<p>I’ve read about this on a few sites but today I’m actually
	  doing it: separating concerns and avoiding anything in the HTML of
	  my website that is presentational.
	<p>It’s awesome!


### 実体参照
不要な実体参照は使用しないこと。
UTF-8においては、—・”・☺のような文字は実体参照を使う必要はない。
HTMLで特別な意味を持つ文字（ < や & など）は例外。

	<!-- NG -->
	The currency symbol for the Euro is &ldquo;&eur;&rdquo;.

	<!-- OK -->
	The currency symbol for the Euro is “€”.


### タグの省略
タグを省略することは原則として禁止。

### type属性
CSSとJavaScriptのtype属性は省略する。
HTML5ではデフォルトの言語として解釈されるため。

	<!-- NG -->
	<link rel="stylesheet" href="//www.google.com/css/maia.css" type="text/css">

	<!-- OK -->
	<link rel="stylesheet" href="//www.google.com/css/maia.css">

	<!-- NG -->
	<script src="//www.google.com/js/gweb/analytics/autotrack.js" type="text/javascript"></script>

	<!-- OK -->
	<script src="//www.google.com/js/gweb/analytics/autotrack.js"></script>


## HTMLの書式ルール
----------

### 全般的な書式
ブロック要素・リスト要素・テーブル要素は改行してから記述し、それらの子要素にはインデントを入れる。
横並びリストなど改行による空白が問題になる場合は、li要素をすべて一行で書いてもOK。

	<blockquote>
	  <p><em>Space</em>, the final frontier.</p>
	</blockquote>


## CSSスタイルルール
----------

### CSSのバリデート
可能な限り適切なCSSを記述すること。
CSSバリデーターにバグがある場合や独自の構文を必要としない限りは、ちゃんと書く。
HTML同様[W3C CSS validator](http://jigsaw.w3.org/css-validator/)などのツールで検証する。

### IDとクラスの命名
IDとクラス名にはちゃんと意味の分かる名前を使うこと。
見た目を反映したものやそれが何を表しているか不可解な名前ではなく、要素の目的や役割を反映した名前を付ける。

	/* NG: 意味が分からん */
	#yee-1901 {}

	/* NG: 見た目を表してる */
	.button-green {}
	.clear {}

	/* OK: 役割を表してる */
	#gallery {}
	#login {}
	.video {}

	/* OK: 汎用的な名前 */
	.aux {}
	.alt {}

### IDとクラスの命名スタイル
意味の分かる範囲でできるだけ短いIDとクラス名を使う。
短くしすぎて意味がわからなくなるようなのはNG。

	/* NG */
	#navigation {}
	.atr {}

	/* OK */
	#nav {}
	.author {}

### タイプセレクタの記述
IDとクラス名にタイプセレクタは記述しない。
パフォーマンスを考慮して不要な子孫セレクタも避ける。

	/* NG */
	ul#example {}
	div.error {}

	/* OK */
	#example {}
	.error {}		

### ショートハンドプロパティ
可能な限りショートハンドでプロパティを書く。

	/* NG */
	border-top-style: none;
	font-family: palatino, georgia, serif;
	font-size: 100%;
	line-height: 1.6;
	padding-bottom: 2em;
	padding-left: 1em;
	padding-right: 1em;
	padding-top: 0;

	/* OK */
	border-top: 0;
	font: 100%/1.6 palatino, georgia, serif;
	padding: 0 1em 2em;

### 「0」と単位
値が「0」なら単位を省略する。

	margin: 0;
	padding: 0;


### URI値の引用符

url()での指定において、”"（ダブルコーテーション）や”（シングルコーテーション）などのURI値の引用符を省略すること。

	@import url(//www.google.com/css/go.css);


### HEX形式のカラーコード

HEX形式のカラーコードで3文字で表記できるものは3文字にする。

	/* NG */
	color: #eebbcc;

	/* OK */
	color: #ebc;

### プレフィックス（接頭辞）

IDやクラス名には固有の接頭辞を付ける。（オプション）
大規模なプロジェクトの場合や、外部サイトに埋め込まれるコードを開発する場合など、IDとクラス名が重複しないように接頭辞（名前空間など）付ける。
接頭辞は後ろにハイフンを付けて繋げる。

	.adw-help {} /* AdWords */
	#maia-note {} /* Maia */

### IDやクラス名の区切り文字

IDやクラス名の別々の単語はハイフンで繋ぐ。

	/* NG: [demo]と[image]が繋がってる */
	.demoimage {}

	/* NG: アンダーバーで繋がってる */
	.error_status {}

	/* OK */
	#video-id {}
	.ads-sample {}

### CSSハック

ユーザーエージェント別の対応のためにCSSハックを使う前に別の方法を試してみること。
CSSハックは、ユーザーエージェントごとの違いを吸収するためには簡単で魅力的な方法だけど、プロジェクト全体のコードの品質を落とすことにもなるので「最後の手段」として考えること。


## CSS書式ルール
----------

### プロパティの記述順序

アルファベットの順に記述する。
ベンダープレフィックスは無視する。ただし、例えば-moz接頭辞は-webkitの前に来る、などの順序は保つこと。

	border: 1px solid;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	border-radius: 4px;
	color: black;
	text-align: center;

### ブロック単位のインデント

その階層がわかるようにブロック単位でコードをインデントする。

	@media screen, projection {
	  html {
	    background: #fff;
	    color: #444;
	  }
	}

### プロパティの終端

すべてのプロパティの終端はセミコロンを書くこと。

	/* NG */
	.test {
	  display: block;
	  height: 100px
	}

	/* OK */
	.test {
	  display: block;
	  height: 100px;
	}

### プロパティ名の終端

すべてのプロパティ名の終端にはコロンの後にスペースを入れること。

	/* NG */
	h3 {
	  font-weight:bold;
	}

	/* OK */
	h3 {
	  font-weight: bold;
	}

### セレクタとプロパティの分離

別々のセレクタとプロパティは改行して書くこと。

	/* NG */
	a:focus, a:active {
	  position: relative; top: 1px;
	}

	/* OK */
	h1,
	h2,
	h3 {
	  font-weight: normal;
	  line-height: 1.2;
	}

### CSSルールの分離

別々のCSSルールは改行して一行間を空けて書く。

	html {
	  background: #fff;
	}

	body {
	  margin: auto;
	  width: 50%;
	}

## CSSメタルール
----------

### セクションのコメント

セクションごとにコメント（任意）を記述する。

	/* Header */
	#adw-header {}

	/* Footer */
	#adw-footer {}

	/* Gallery */
	.adw-gallery {}


## 最後に

「一貫性を持ちましょう。」

コードを編集する前に、あなたは周りのコードを見て、そのスタイルに沿って書きましょう。算術演算子の前後にスペースがあるなら、あなたもそうすべきです。コメントの周りが#で囲まれているなら、そうすべきです。

スタイルガイドラインを持つことのポイントは、コーディングの共通の語彙を持つことです。ここでは、グローバルなスタイルルールを提示していますが、ローカルなルールも重要です。もしあなたが追加したコードのスタイルが、周りの既存のコードと大幅に異なっていると、他人がそのコードを触るとき、リズムが崩れてしまいます。このようなことは避けましょう。

## 参考
----------

* [Google HTML/CSS Style Guide](http://google-styleguide.googlecode.com/svn/trunk/htmlcssguide.xml)
* [「Google HTML/CSS Style Guide」を適当に和訳してみた | REFLECTDESIGN](http://re-dzine.net/2012/05/google-htmlcss-style-guide/)