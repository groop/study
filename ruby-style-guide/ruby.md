# Rubyのコーディング規約
----------

## はじめに
----------

### 対象バージョン
1.9.3系を対象とします。


## ソースコードの整形
----------

### インデント
インデントには半角スペースを使用し、幅は2とします。

### 一行の桁数
一行の桁数は80桁までを目安とします。原則として、最大で130桁以内とします。

### 空行
複数のクラスの区切には空行を挿入する。

	class Foo
	  ...
	end

	class Bar
	  ...
	end


また、クラス内の各構成要素の区切にも空行を挿入する。ただし、最初の構成要素の前や、最後の構成要素の後には空行は挿入しない。

	#良い例
	class Foo
	  attr :bar
	
	  def baz
	    ...
	  end
	
	  def quux
	    ..
	  end
	end

	#悪い例
	class Foo

	  attr :bar
	
	  def baz
	    ...
	  end
	
	  def quux
	    ...
	  end
	
	end


### コメント
メソッド内のコメントは最小限に留める。

クラス・モジュールやパブリックメソッドにはyard形式で記述する。

書式は以下のサイトを参考に。


* [Rakeタスクへの登録の仕方とメモ](http://namakesugi.blog42.fc2.com/blog-entry-125.html)
* [Documentation for yard (0.8.6.1)](http://rubydoc.info/docs/yard/file/docs/GettingStarted.md)


### 改行コード
改行コードはLinux(LF)とします。

### 文字コード
ファイルのエンコーディングは UTF-8 とします。 



## 構文に関する規約
----------

### クラスの構成要素
クラスの構成要素は以下の順序で記述する。

1. モジュールのインクルード
2. 定数の定義
3. クラス変数・クラスのインスタンス変数の定義
4. パブリックなクラスメソッドの定義
5. アクセサの定義
6. initializeの定義
7. パブリックなインスタンスメソッドの定義
8. プロテクティッドなクラスメソッドの定義
9. プロテクティッドなアクセサの定義
10. プロテクティッドなインスタンスメソッドの定義
11. プライベートなクラスメソッドの定義
12. プライベートなアクセサの定義
13. プライベートなインスタンスメソッドの定義
14. ネストしたクラスの定義


### アクセサの定義
アクセサの定義には、attr_accessor・attr_reader・attr_writerを使用する。(attrは使用しない)


### クラスメソッドの定義
クラスメソッドの定義にはselfを使用する。

	#悪い例
	class Foo
	  def Foo.foo
	    ...
	  end
	end

	#良い例
	class Foo
	  def self.foo
	    ...
	  end
	end


### メソッドの定義
メソッド定義のとき、引数が無い場合は括弧を省略し、ある場合は括弧を記述するようにします。

	#悪い例
	def some_method()
	end

	def some_method_with_arguments arg1, arg2
	end

	#良い例
	def some_method
	end

	def some_method_with_arguments(arg1, arg2)
	end


メソッドの仮引数に値を代入するとき、= の両端にスペースを入れます。

	# 悪い例
	def some_method(arg1=:default, arg2=nil, arg3=[])
	end

	# 良い例
	def some_method(arg1 = :default, arg2 = nil, arg3 = [])
	end


オプション引数としてハッシュを使用するのは極力避けます。

	def plot(x, y=nil, options=nil)
	   
	   if options
	      if (val=options[:xtitle]);  DCL.uscset("cxttl", val); end
	      if (val=options[:ytitle]);  DCL.uscset("cyttl", val); end
	   end
	end



### メソッド呼び出し

メソッド呼び出しの引数リストには括弧を付ける。

	#良い例
	obj.foo(1, "abc")
	
	#悪い例
	obj.foo 1, "abc"


ただし、引数がない場合は、括弧を省略する。

	#良い例
	bar
	
	#悪い例
	bar()


printやputsやpの 場合は、引数の括弧を省略してもよい。

	#良い例
	print "x = ", x, "\n"


メソッド名と開始括弧 ( ( ) の間にスペースを入れてはいけません。もし第一引数で括弧を使用していた場合、メソッドの括弧を必ず入力します。

	# 悪い例1 (メソッド名と開始括弧の間にスペースが入っている)
	p (3 + 2) + 1

	# 悪い例2 (メソッド名と開始括弧の間にスペースは入っていないが、第一引数で括弧が使用されている)
	p(3 + 2) + 1

	# 良い例 (第一引数で括弧を使用していた場合、メソッドの括弧を必ず入力する)
	p((3 + 2) + 1)



### ブロック
1行で記述されるブロックには中括弧 {...} を使用します。その1行が複雑だったり横に長かったりする場合や、処理を複数行記述するときは、do...end を使用します。

	names = ['Bozhidar', 'Steve', 'Sarah']
	
	# 良い例1
	names.each { |name| puts name }

	# 悪い例1 (1行で簡潔な処理を do...end 内に記述している)
	names.each do |name|
	  puts name
	end

	# 悪い例2 (複雑で横に長い処理を1行で記述している)
	names.each { |name| puts sugoku.nagai.shori(fukuzatsu.na.shori(name)) }

	# 良い例2 (複雑な処理は複数行に分割する。複数行になるときは do...end を使用する)
	names.each do |name|
	  name = fukuzatsu.na.shori(name)
	  name = sugoku.nagai.shori(name)
	  puts name
	end


ブロックに対してメソッドチェインするときは、中括弧 {...} で囲まれたブロックに対して行います。

	# 良い例
	names.select { |name| name.start_with?("S") }.map { |name| name.upcase }

	# 悪い例
	names.select do |name|
	  name.start_with?("S")
	end.map { |name| name.upcase }


使用しないブロック引数名は _ にします。

	# 悪い例
	result = hash.map { |k, v| v + 1 }

	# 良い例
	result = hash.map { |_, v| v + 1 }


### return

メソッドの値を返す場合は、必ずreturnを使用する。 また、returnの括弧は省略する。

	#悪い例
	def add(x, y)
	  return(x + y)
	end
	
	#良い例
	def add(x, y)
	  return x + y
	end

return が不要なときは記述しません。

	# 悪い例
	def some_method(some_arr)
	  return some_arr.size
	end

	# 良い例
	def some_method(some_arr)
	  some_arr.size
	end



### yield

yieldの呼び出し方法はメソッド呼び出しに準じます。


### 条件分岐
if/unless 式には then を付加せず記述します。

	# 悪い例
	if some_condition then
	end
	
	# 良い例
	if some_condition
	end


if 式の else 句が必要なく、1行で記述できる場合は、後置 if/unless を使用します。

	# 悪い例
	if some_condition
	end
	
	# 良い例
	do_something if some_condition


条件式に否定(!)は使用せず、unless 式を使用します。

	# 悪い例
	do_something if !some_condition
	
	# 良い例
	do_something unless some_condition


unless 式では else 句を使用しません。if 式に書き換えます。

	# 悪い例
	unless success?
	  puts 'failure'
	else
	  puts 'success'
	end
	
	# 良い例
	if success?
	  puts 'success'
	else
	  puts 'failure'
	end


if/unless/while 式の条件部には括弧を使用しません。

	# 悪い例
	if (x > 10)
	end
	
	# 良い例
	if x > 10
	end


if 式の条件部では変数の代入を行いません。

	# 悪い例1
	if (v = array.grep(/foo/)) ...
	
	# 悪い例2
	if v = array.grep(/foo/) ...

	# 悪い例3
	if (v = self.next_value) == "hello" ...

	# 良い例
	v = self.next_value

	if v == 'hello' ...


caseを使用できる場合は、caseを使用する。 また、thenは省略する。

	#良い例
	case x
	when 1
	  ...
	when 2
	  ...
	end
	
	#悪い例
	if x == 1
	  ...
	elsif x == 2
	  ...
	end


条件分岐の式の値は使用しない。

	#悪い例
	msg = if x > 0
	        "x > 0"
	      else
	        "x <= 0"
	      end

	#良い例
	if x > 0
	  msg = "x > 0"
	else
	  msg = "x <= 0"
	end


case 式と when 節のインデントは同じ深さにします。 

	# 良い例1
	case
	when song.name == 'Misty'
	  puts 'Not again!'
	when song.duration > 120
	  puts 'Too long!'
	else
	  song.play
	end

	# 悪い例
	case
	  when song.name == 'Misty'
	    puts 'Not again!'
	  when song.duration > 120
	    puts 'Too long!'
	  else
	    song.play
	end


### 繰り返し
whileのdoは省略する。

	#悪い例
	while cond do
	  ...
	end

	#良い例
	while cond
	  ...
	end


while !xのような場合は、 until xに置き換える。

	#悪い例
	while !cond
	  ...
	end

	#良い例
	until cond
	  ...
	end


できるだけ for 式を使用せず、each メソッドを使用します。

	arr = [1, 2, 3]
	
	# 悪い例
	for elem in arr do
	  puts elem
	end
	
	# 良い例
	arr.each { |elem| puts elem }


無限ループにはloopを使用する。

	#悪い例
	while true
	end

	#良い例
	loop do
	  ...
	end


### 論理演算子
論理演算には ! や && や || を使用する。 (not/and/orは使用しない。)

### 三項演算子

if/then/else/end が1行で書けるような処理の場合は、代わりに三項演算子を使用します。

	# 悪い例
	result = if some_condition then something else something_else end
	
	# 良い例
	result = some_condition ? something : something_else



三項演算子がネストするようなときは、外側の条件分岐を if/else で記述します。

	# 悪い例
	some_condition ? (nested_condition ? nested_something : nested_something_else) : something_else

	# 良い例
	if some_condition
	  nested_condition ? nested_something : nested_something_else
	else
	  something_else
	end



### 文字列リテラル
文字列の結合は使用せず、式展開を使用します。

	# 悪い例
	email_with_name = user.name + ' <' + user.email + '>'

	# 良い例
	email_with_name = "#{user.name} <#{user.email}>"


式展開やバックスラッシュ記法( \t や \n など) を使用しない場合はシングルクォートを使用します。

	# 悪い例
	name = "Bozhidar"

	# 良い例
	name = 'Bozhidar'


大きな文字列を結合する場合は、String#+ を使用せず String#<< を使用します。

	html = ''
	html << '<h1>Page title</h1>'

	paragraphs.each do |paragraph|
	  html << "<p>#{paragraph}</p>"
	end

1行で記述できる文字列で、ダブルクォートと式展開を両方使用している場合は %() を使用します。

	# 悪い例 (ダブルクォートが記述されていない)
	%(This is #{quality} style)
	# "This is #{quality} style" が好ましい
	
	# 悪い例 (式展開が記述されていない)
	%(<div class="text">Some text</div>)
	# '<div class="text">Some text</div>' が好ましい
	
	# 悪い例 (ダブルクォートが記述されていない)
	%(This is #{quality} style)
	# "This is #{quality} style" が好ましい
	
	# 悪い例 (複数行の文字列になっている)
	%(<div>\n<span class="big">#{exclamation}</span>\n</div>)
	# ヒアドキュメントを使用するのが好ましい
	
	# 良い例 (ダブルクォートと式展開が記述されている)
	%(<tr><td class="name">#{name}</td>)


複数行に渡る場合はヒアドキュメントを使用します。

	print <<EOS
	  the string
	  next line
	EOS

ただし、複数行の文字列を扱う場合はインラインではなく、erbなどのテンプレートの利用を検討すること。


### 多重代入
パブリックメソッドでの使用は原則として禁止。プライベートメソッドでも多様は避けること。


### 変数の初期化

すでに値が代入されているかもしれない変数の初期化には ||= を使用します。

	name ||= 'Bozhidar'

ただし、真偽値の初期化を行うときは ||= を使用してはいけません。

	# 悪い例
	enabled ||= true

	# 良い例
	enabled = true if enabled.nil?


### 配列、ハッシュ
ハッシュはRuby 1.9から追加されたリテラルを使用して記述します。

	# 悪い例
	hash = { :one => 1, :two => 2 }

	# 良い例
	hash = { one: 1, two: 2 }
	

文字列が格納されるだけの配列の場合は、 %w を使用します。

	# 悪い例
	STATES = ['draft', 'open', 'closed']

	# 良い例
	STATES = %w(draft open closed)



### lamda

lambda メソッドをリテラルで記述します。

	# 悪い例
	lambda = lambda { |a, b| a + b }
	lambda.call(1, 2)

	# 良い例
	lambda = ->(a, b) { a + b }
	lambda.(1, 2)


### 正規表現


### パーセント記法


### 例外処理


### メタプログラミング


## 命名規則
----------

### 全般

1. 原則として、単語の省略は行わない。
2. スコープが狭いループ変数には、i, j, kという名前をこの順序で使用する。
3. スコープが狭い変数名には、クラス名を省略したものを使用してよい。 (例: eo = ExampleObject.new)


予約語を** $, @、@@ **などをつけて無理やり変数に使用するのは禁止とします。
** !, ? **などでのメソッド化も禁止。

	BEGIN    class    ensure   nil      self     when
	END      def      false    not      super    while
	alias    defined? for      or       then     yield
	and      do       if       redo     true     __LINE__
	begin    else     in       rescue   undef    __FILE__
	break    elsif    module   retry    unless   __ENCODING__
	case     end      next     return   until


##### 参考
[リファレンスマニュアル - 予約語](http://doc.ruby-lang.org/ja/1.9.3/doc/spec=2flexical.html#reserved)


### クラス・モジュール名
クラスとモジュールには CamelCase を使用します。 ただし、HTTP, XMLのような単語の頭文字を取った名前はそのまま全て大文字にします。

	#悪い例
	Example_Class
	EXAMPLE_CLASS
	HttpClient
	HTTPclient
	HTTP_Client

	#良い例
	ExampleClass  
	HTTPClient


### メソッド名
メソッド名には snake_case を使用します。

	#悪い例
	addsSomething
	Add_Something

	#良い例
	add_something


真偽値を返すメソッド名は、動詞または形容詞に ? を付け、形容詞に  is_ は付けない。

	#悪い例
	is_visible
	is_visible?

	#良い例
	visible?


self を書き換えるような破壊的なメソッドの名前には、最後に ! を付加します。

破壊的なメソッドを定義したとき、可能であれば、同じ返り値を返す非破壊的なメソッドも定義します。

	class Array
	  def flatten_once!
	    res = []
	      
	    each do |e|
	      [*e].each { |f| res << f }
	    end
	
	    replace(res)
	  end
                          
	  def flatten_once
	    dup.flatten_once!
	  end
	end

エイリアスが存在するメソッドは、どちらか一方を使用します。例えば、


* collect より map
* detect より find
* find_all より select
* inject より reduce
* length より size



### 変数名
変数名には snake_case を使用します。


### 定数名
定数には SCREAMING_SNAKE_CASE を使用します。


### ファイル名


## その他
-----------

### ツール
RuboCopによるソースコードの評価を行う

* [RuboCop](https://github.com/bbatsov/rubocop)


### 開発環境
動作テストにはプロジェクトごとにvagrantで動作するVMの配布を行う。


### フレームワーク
これらに加えて、使用するフレームワークの規約も参照のこと。

* [Rails規約](rails.md)


### 参考
* [Ruby リファレンスマニュアル](http://doc.ruby-lang.org/ja/1.9.3/doc/index.html)
* [コーディング規約をまとめてみた (Ruby編) - bojovs::blog](http://bojovs.github.io/2012/04/24/ruby-coding-style/)