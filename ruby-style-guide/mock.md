# Mock作成ガイドライン
----------

## はじめに
----------

### 基本情報
以下の様な構成での作成となります。

* Ruby on Rails3.2
* Slim 1.3.9
* Sass 3.2.9
* CoffeeScript 1.6.2
* Twitter Bootstrap 


### セットアップ(Windowsの場合)

1. RailsInstallerの実行
2. node.jsのインストール

※実際の環境とRubyのバージョンの違いが発生するが、VMでの動作確認ができれば問題ない

* [(マジに) Windows で行う Rails 開発 (翻訳版) | Engine Yard Blog JP](http://www.engineyard.co.jp/blog/2012/rails-development-on-windows-seriously/)
* [RailsInstaller](http://railsinstaller.org/)
* [node.js](http://nodejs.org/)


### セットアップ(Mac OS X)
参考サイトを元に適当に入れれば良いかと。

バージョン切り替えが出来るようにebenv経由で入れること。

* [Homebrew + rbenv + ruby-buildでruby 2.0.0-p0をインストール #Ruby - Qiita [キータ]](http://qiita.com/items/b38b330b44f517f3c77c)


### bundle
bundlerにより必要なgemファイルをインストールしてください。

	bundle install

この作業はgemの構成が変更されるたびに実行が必要になります。以下の様なメッセージが表示された場合は再度実行して下さい。

	Could not find gem 'awelrkweajre (>= 0) ruby' in the gems available on this machine.
	Run `bundle install` to install missing gems.


### アプリケーションのセットアップと起動
ベースとなるプロジェクトにセットアップタスクを用意してあります。

	# ベースプロジェクトのclone
	git clone git@your-server:you/your-repo.git
	
	# セットアップ
	rake setup

**rake setup**はDBの再構築を行うので、DBの構成がおかしくなった場合にも使用出来ます。


起動は以下のコマンドで行えます。

	# 起動
	rails server

その後、[http://localhost:3000/](http://localhost:3000/)でアクセスできます。


## Slim
----------
HTMLのテンプレートエンジンです。

### Slimの参考サイト

* [ドキュメント](http://rdoc.info/gems/slim/frames)
* [ドキュメント(翻訳)](https://github.com/yterajima/slim/blob/README_ja/README.md)

## Sass
----------
CSSのテンプレートエンジンです。

### sassとscss
*.sass ではなく *.scssでの記述を推奨します。


### Sassの参考サイト

* [Sass チュートリアル](http://hail2u.net/documents/sass-tutorial.html)
* [Sassで行こう](http://hail2u.net/documents/ala-getting-started-with-sass.html)
* [Sass リファレンス](http://sass-lang.com/docs/yardoc/file.SASS_REFERENCE.html)


## CoffeeScript
JavaScriptのテンプレートエンジンです。

### CoffeeScriptの参考サイト

* [CoffeeScript 言語リファレンス](http://memo.sappari.org/coffeescript/coffeescript-langref)



## Rails
----------

### コントローラとアクション
コントローラとアクションは以下のコマンドから作成して下さい。

	rails g controller コントローラ名 [アクション名1 アクション名2 …]

作成済みのコントローラにアクションを追加する場合は以下の様な操作が必要です。

##### アクションの定義
既存のコントローラに対してアクションの追加を行います。

	# vi app/controllers/コントローラ名_controller.rb
	
	class コントローラ名Controller < ApplicationController
		def index
		end

		def 追加するアクション名
		end
	end

##### ビューの作成
次に、ビューの作成を行います。

	# vi app/views/top/index.html.slim

	h1 新しいアクション	

##### その他
固有のCSS・JSを適用する場合は** CSS ** ** JavaScript **
の項を参照して下さい。
	

### レイアウト

#### 全ページ共通のレイアウト
全ページ共通のレイアウトを記述する場合は以下のファイルに行います。

	app/views/layouts/application.html.slim


#### レイアウトの追加
別のレイアウトが必要な場合はgenerateコマンドを使用して下さい。

	rails generate bootstrap:layout レイアウト名 fluid

これにより以下のファイルが作成されますので、ページ固有のレイアウトを記述して下さい。

	app/views/layouts/レイアウト名.html.slim

** ただし、レイアウトをたくさん作るのは厳禁です。**



### CSS

#### 全ページ共通のスタイル

全ページ共通のスタイルを記述する場合は以下のファイルに行います。

	app/assets/stylesheets/application.css.scss


#### レイアウト共通のスタイル

レイアウト共通のスタイルを記述する場合はレイアウトファイルに以下のタグを記述して下さい。

	= stylesheet_link_tag "layout_レイアウト名", :media => "all"

スタイルの記述は以下のファイルに行います。

	vi app/assets/stylesheets/layout_レイアウト名.css.scss


#### コントローラ共通のスタイル

ページ固有のスタイルを記述する場合は以下のファイルに行います。

	app/assets/stylesheets/コントローラ名.css.scss

また、気をつけるべきなのは ** assets **以下のファイルはすべてのページで読み込まれる点です。
ページ固有のスタイルを適用する場合、bodyタグのclassにコントローラ・アクション名が自動的に埋め込まれるので、これにより局所化して下さい。

	# 例 (controller=sample::books action=index)
	<body class="body-sample-books body-sample-books_index body-index layout-sample">

scssの場合、以下のよう記述すれば局所化できます。

	.body-sample-books {
		# この中に固有のスタイルを記述
		h1 { font-size: 20px }
	}


### JavaScript

#### 全ページ共通のスクリプト
前頁共通のスクリプトを記述する場合は以下のファイルに行います。

	app/assets/javascript/application.js.coffee


#### コントローラ共通のスクリプト

ページ固有のスクリプトを記述する場合は以下のファイルに行います。

	app/assets/javascripts/コントローラ名.js.coffee

また、cssと同様にファイル自体はすべてのページで読み込まれるので、こちらもクラス名で局所化してください。


	

### フォーム
----------

#### formタグ
モックの段階ではモデル未定義の状態なので、form_tag メソッドを使用して下さい。

* [フォーム(form) - Railsドキュメント](http://railsdoc.com/form)


#### 構造
TwitterBootstrapの定義済み構造を使用して下さい。

* [Base · Bootstrap](http://twitter.github.io/bootstrap/base-css.html)

また、サンプルページが用意してありますので、そちらを参考にして下さい。

* [http://localhost:3000/](http://localhost:3000/sample/)


### 各種ヘルパー
----------

#### 特殊文字のエスケープ
変数を出力する場合、HTMLの特殊文字は自動的にエスケープされます。

エスケープを行いたくない場合はrawメソッドを使用して下さい。

	= raw(@text)


#### リンク
リンクを配置したい場合は link_to メソッドを使用して下さい。

	link_to('表示名', '/books/index')

* [aaa](<a> - HTMLタグ逆引き - Railsドキュメント)
* [link_toメソッドを使ったリンクの作成 - Ruby on Rails入門](http://www.rubylife.jp/rails/template/index8.html)



#### 画像
imgタグを配置したい場合は image_tag メソッドを使用して下さい。

	= image_tag('image.png')

* [image_tagメソッドを使ったイメージタグの作成 - Ruby on Rails入門](http://www.rubylife.jp/rails/template/index11.html)



#### その他ヘルパー
各種タグを出力するタグを積極的に使用して下さい。

* [HTMLタグ逆引き - Railsドキュメント](http://railsdoc.com/htmls/)