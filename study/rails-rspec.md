# RSpecメモ
自分用のRSpec関係のメモです。
[「Everyday Rails - RSpecによるRailsテスト入門」](http://blog.jnito.com/entry/2014/02/07/110505)からの抜粋がほとんどですが、自分のプロジェクトに合わないところは修正・加筆しています。


# 準備
この章に関することは、プロジェクト開始時に一度行うだけで良いことです。
あなたがアプリケーションの立ち上げを行うのでなければほとんど読み飛ばしてしまって構いません。


## Gemfile
基本要素としては以下を追記すれば良い。

    group :development, :test do

      # RSpec
      gem "rspec-rails", "~> 2.14.0"

      # テストデータの作成
      gem "factory_girl_rails", "~> 4.2.1"
    end

    group :test do

      # テストデータの作成補助
      gem "faker", "~> 1.1.2"

      # Capybara
      gem "capybara", "~> 2.1.0"

      # データベースの初期化
      gem "database_cleaner", "~> 1.0.1"

      # デバッグのサポート(save_and_open_page)
      gem "launchy", "~> 2.3.0"

      # seleniumによるテスト実行サポート
      gem "selenium-webdriver", "~> 2.35.1"
    end


## RSpecの設定
RSpecのインストールは以下の様なコマンドで行える。

    bundle exec rails generate rspec:instal

### .rpsec
RSpecのインストールによって様々なファイルが自動生成されるが、
その中に *.rspec* というファイルがある。

これは、rspec実行時に渡されるパラメータの初期値を定義するものである。
主に出力形式の変更を行う目的で使用することになるだろう。

例えば、以下のように記述すると、rspecの結果が変化することが確認できる。

    # .rspec
    --format documentation


## ジェネレータ
generateコマンドによるファイルの生成をコントロールできる。不必要なファイルは生成しない。

    config.generators do |g|
      g.test_framework :rspec,
        fixtures: true,
        view_specs: false, 
        helper_specs: false,
        routing_specs: false,
        controller_specs: true,
        request_specs: false,
        g.fixture_replacement :factory_girl, dir: "spec/factories"
    end

* ビューのテストは総合テストでカバーするので必要ない。
* routingは規模が複雑なルーティングが必要ならば検討する。
* 余裕ができたらhelperも作る。
* 必要になったら手動で作れば良い。


## テスト環境のデータベース
データベースの作成は以下のコマンドで出来る。これは初回に一度だけ行えば良い。

    bundle exec rake db:create:all

マイグレートした後にはクローンすること。

    bundle exec rake db:test:clone


## rails new のテンプレート
新規プロジェクトを作る際のテンプレート。基本的には必要なさそうだけど、一度目を通しておきたい。

* [RailsWizard](http://railswizard.org/)
* [App Scrolls](http://appscrolls.org/)


## specディレクトリの構造
app以下と同じ構造にすること。自動生成されるファイル類はそのルールに則って作られる。
scaffoldなどで生成ルールを見てみると良い。


## shouldとexpect
古い資料だとshouldを使っているものもあるが、作成する際はすべてexpectに置き換えること。
shouldは将来的には何らかの手続を行わないと使えなくなる。

マッチャは共通なので構文が違うだけ。

    # shouldは×
    it "is true when true" do
      true.should be_true
    end

    # expectは◯
    it "is true when true" do

      # expect A to do （Aに do を期待する）
      expect(true).to be_true

    end

後に例としてもでるが、文脈によってはshouldが良いとされる場合もある。可読性を優先するのが良い。



## RSpecの実行
以下のコマンドでRSpecの実行が行える。
実際に開発する際には *Guard* による実行が主になるが、それについては後述する。

    bundle exec rspec


# モデルスペック
モデルのテストは以下の項目が主なテスト対象となる。

* バリデーション
* クラスメソッド
* インスタンスメソッド

これは次のように言い換えられる。

* 有効な属性が渡された場合にcreateメソッドが発行できること
* バリデーションを失敗させるデータを渡した時に、有効でないこと
* クラスメソッドとインスタンスメソッドが期待通りに動作すること


## モデルスペックの基本構造
まずはモデルスペックのアウトラインを定義してみる。

    require 'spec_helper'

    describe Contact do
      # 姓と名とメールがあれば有効な状態であること
      it "is valid with a firstname, lastname and email"

      # 名がなければ無効な状態であること
      it "is invalid without a firstname"

      # 姓がなければ無効な状態であること
      it "is invalid without a lastname"

      # メールアドレスがなければ無効な状態であること
      it "is invalid without an email address"

      # 重複したメールアドレスなら無効な状態であること
      it "is invalid with a duplicate email address"

      # 連絡先のフルネームを文字列として返すこと
      it "returns a contact's full name as a string"
    end

以下の点について注意してほしい。

* 期待する結果を記述(describe)している
* example(itで始まる1行)につき、一つの結果を期待(expect)している
  * 細かく分類しておけばエラー時の原因特定が結果出力だけで行える
* どのexampleも明示的である
  * itの後に続く説明は省略できるが、書いておいたほうがわかりやすい



## バリデーションのテスト
次に、先ほどのアウトラインに対してバリデーションに関するテストの例を幾つか実装してみる。

### 有効な状態である be_valid

    # 姓と名とメールがあれば有効な状態であること
    it "is valid with a firstname, lastname and email" do
      contact = Contact.new(
        firstname: 'Aaron',
        lastname: 'Sumner',
        email: 'tester@example.com')
      expect(contact).to be_valid
    end


### 属性がエラーである errors_on

    # 名がなければ無効な状態であること
    it "is invalid without a firstname" do
      expect(Contact.new(firstname: nil)).to have(1).errors_on(:firstname)
    end


### サンプルメールアドレスの重複チェック
it内のcreateでDBに作成し、newで作ったインスタンスの検証を行っている。

    # メールアドレスが重複する場合は無効な状態である
    it "is invalid with a duplicate email address" do
      Contact.create(
        firstname: 'Joe',
        lastname: 'Tester',
        email: 'tester@example.com')

      contact = Contact.new(
        firstname: 'Jane',
        lastname: 'Tester',
        email: 'tester@example.com')

      expect(contact).to have(1).errors_on(:email)
    end


## インスタンスメソッドのテスト
例えば、firstnameとlastnameを元にフルネームを返すようなインスタンスメソッドがある場合は以下のようになる。

    # 連絡先のフルネームを文字列として返すこと
    it "returns a contact's full name as a string" do
      contact = Contact.new(
        firstname: 'John',
        lastname: 'Doe',
        email: 'johndoe@example.com')
       expect(contact.name).to eq 'John Doe'
    end

等値のエクスペクテーションを書くときは == より eq を使う方が良い。


## クラスメソッドとスコープのテスト
与えた頭文字と一致する名前を返す様なクラスメソッドがある場合は以下のようになる。

    # マッチした結果をソート済みの配列として返すこと
    it "returns a sorted array of results that match" do
      smith = Contact.create(
        firstname: 'John',
        lastname: 'Smith',
        email: 'jsmith@example.com')

      jones = Contact.create(
        firstname: 'Tim',
        lastname: 'Jones',
        email: 'tjones@example.com')

      johnson = Contact.create(
        firstname: 'John',
        lastname: 'Johnson',
        email: 'jjohnson@example.com')

      expect(Contact.by_letter("J")).to eq [johnson, jones]
    end


### 失敗をテストする
使用するマッチャに一致しないことを確認する場合は以下のようになる。

    # マッチした結果をソート済みの配列として返すこと
    it "returns a sorted array of results that match" do
      smith = Contact.create(
        firstname: 'John',
        lastname: 'Smith',
        email: 'jsmith@example.com')

      jones = Contact.create(
        firstname: 'Tim',
        lastname: 'Jones',
        email: 'tjones@example.com')

      johnson = Contact.create(
        firstname: 'John',
        lastname: 'Johnson',
        email: 'jjohnson@example.com')

      expect(Contact.by_letter("J")).to_not include smith
    end

この例は前項のテストで充分なので適切とは言えないが、 **起こってほしいこと** はもちろん **起こってほしくないこと**をテストするのも重要である。


## マッチャ(matcher)について
標準のマッチャに加えてプロジェクトごとにgemで追加しているだろう。
通常はそのプロジェクトのREADMEに資料へのリンクなどを記載するだろうが、見つからないようであればGemfileを参照すると良い。

また、yardocによるドキュメントも参照すると良い。

    # yardocの実行
    bundle exec yard server --gems

* [RSpecの標準Matcher一覧表 - 本当は怖い情報科学](http://d.hatena.ne.jp/keisukefukuda/20080124/p1)


## モジュールのテスト
規模が大きくなると適度にモジュールに分解することも多くある。
それらをinclude先のモデルに対してテストすることも可能だが、
可読性も考慮するとモジュール単位でテストを行いたい。

これに対する回答は後述する。


# rspecのブロック構造(describe、context、before、after)
describeは入れ子にすることが可能。うまくグルーピングして読みやすいようにする。

    describe Contact do

      # 文字で姓をフィルタする
      describe "filter last name by letter" do
        before :each do
          @smith = Contact.create(firstname: 'John', lastname: 'Smith', email: 'jsmith@example.com')
          @jones = Contact.create(firstname: 'Tim', lastname: 'Jones', email: 'tjones@example.com')
          @johnson = Contact.create(firstname: 'John', lastname: 'Johnson', email: 'jjohnson@example.com')
        end

        # マッチする文字の場合
        context "matching letters" do
          it "returns a sorted array of results that match" do
            expect(Contact.by_letter("J")).to eq [@johnson, @jones]
          end
        end

        # マッチしない文字の場合
        context "non-matching letters" do
          it "returns a sorted array of results that match" do
            expect(Contact.by_letter("J")).to_not include @smith
          end
        end
      end
    end


## describeとcontext
describeとcontextは機能的には同等のもの。
筆者はdescribeはクラスの機能に関するアウトラインとして、contextは特定の状態に関するアウトラインとして使い分けている。


## beforeとafter
beforeとafterはそれぞれ、サンプルブロックの前または後に自動的に実行される。
実行されるタイミングは第一引数で与えるものによって変化する。
種類は以下のとおり。

* each : すべてのexample毎
* all : exampleグループ毎
* suite : 全体で一回(RSpec.configureでのみ使用可)

:eachが初期値になっているので、省略すればすべてのexample前に実行されるが、基本的には明示的に記述すること。


## 境界値をテストする
例えば、入力文字数の検証が必要だった場合、範囲内のデータのみではなく、範囲外のデータでもテストすべきである。


# テストデータの生成
rails標準のfixtureは単純なデータのリストを取り込むのは良いが、リレーション等を表現するのが面倒でわかりづらい。
代替策として、代わりにFactoryGirlを利用する。


## factoryデータの配置
factoryを記述するファイルは **spec/factories/モデル名(複数形).rb** に配置する。


## factoryの作成
*spec/factories/contacts.rb* を作成して以下のように記述する。

    FactoryGirl.define do
      factory :contact do
        firstname "John"
        lastname "Doe"
        sequence(:email) { |n| "johndoe#{n}@example.com"}
      end
    end

*sequence* は連番付与。一意なものに利用すると良い。


## factoryを使ったテストデータの生成
代表的なものを幾つか挙げてみる。

    # インスタンスの生成(Model.new)
    contact = FactoryGirl.build(:contact)

    # データの挿入(Model.create)
    FactoryGirl.create(:contact)

    # 属性値をハッシュで生成
    FactoryGirl.attributes_for(:contact)

    # DBには登録しないけど、ID属性は設定される
    FactoryGirl.build_stubbed(:contact)


また、生成する際に必要な属性をオーバーライドすることも可能。

    FactoryGirl.build(:contact, firstname: nil)


RSpec.configureにて、以下のようにすることでシンプルな構文を書くこともできる。

    RSpec.configure do |config|
      # ファクトリを簡単に呼び出せるよう、Factory Girl の構文をインクルードする
      config.include FactoryGirl::Syntax::Methods
    end

    # FactoryGirlを省略できる
    create(:contact)
    build(:contact)


### 関連するモデルの定義
*association* は関連するモデルを自動的に作成します。

    FactoryGirl.define do
      factory :phone do
        association :contact
        phone { '123-555-1234' }
        phone_type 'home'
      end
    end

また、build時にモデルを渡した場合にはそのオブジェクトが使用される。

    contact = create(:contact)
    create(:phone, contact: contact)


### factoryの構造
factoryを入れ子にすることで継承することができる。

    FactoryGirl.define do
      factory :phone do

        association :contact
        phone { '123-555-1234' }
        factory :home_phone do
          phone_type 'home'
        end
        factory :work_phone do
          phone_type 'work'
        end
        factory :mobile_phone do
          phone_type 'mobile'
        end
      end
    end

この場合、3種類のデータを作成することができる

    FactoryGirl.create(:home_phone)
    FactoryGirl.create(:work_phone)
    FactoryGirl.create(:mobile_phone)


### リアルなダミーデータの作成
**Faker** を使ってリアルなダミーデータの作成ができる。

* [Faker - Rubydoc](http://rubydoc.info/gems/faker/1.0.1/frames)
* [gems: faker, gimei, romankana | deadwood](http://www.d-wood.com/blog/2013/11/04_4984.html)
* [Rails3+Ruby1.9.2でFaker::Japanese+RomanKanaで日本語名を生成 | tomotaka-itoの日記](http://tmtk.org/blog/2011/04/171)


### コールバック
もっと複雑なデータの場合はコールバックを利用する。
例えば、連絡先情報には必ず３つの電話番号が登録されている必要がある場合は以下のようになる。

    require'faker'

    FactoryGirl.define do
      factory :contact do
        firstname { Faker::Name.first_name }
        lastname { Faker::Name.last_name }
        email { Faker::Internet.email }
        after(:build) do |contact|
          [:home_phone, :work_phone, :mobile_phone].each do |phone|
            contact.phones << FactoryGirl.build(:phone, phone_type: phone, contact: contact)
          end
        end
      end
    end

コールバックには他にも種類がある。

* after(:build)
  : build後に呼び出される(結果的に、buildとcreateの両メソッドで呼び出される)
* before(:create)
  : DB登録前に呼び出される
* after(:create)
  : DB登録後に呼び出される

もっと知りたい場合は次の記事を参照すること。

* [Get Your Callbacks On with Factory Girl 3.3](http://robots.thoughtbot.com/get-your-callbacks-on-with-factory-girl-3-3)


## その他のテストデータ作成
FactoryGirlは遅くなる原因にもなりやすい。
詳細は後述するが、通常は無視して良い。

* モック(mocks)
* スタブ(stubs)


## 参考記事
* [Rails - Factory Girl 3.x メモ - Qiita](http://qiita.com/torshinor/items/383691344ec4a2233fe5)
* [FactoryGirl公式ドキュメント](https://github.com/thoughtbot/factory_girl/blob/master/GETTING_STARTED.md)



# コントローラのテスト
Railsのプロジェクトにおいて、コントローラのテストを行わないこともあるが、
基本的には実装することを推奨する。

### なぜコントローラをテストしないのか
* コントローラは薄い
* フィーチャースペック(総合テスト)でまかなえる
* フィーチャスペックよりも速いとは言え、コントローラスペックもモデルや普通のRubyオブジェクトのスペックよりは遅い。

### なぜコントローラのテストをするのか
* コントローラもメソッドを持ったクラスである
* コントローラスペックは統合テスト用のスペックで同じ事をやるのに比べて速く書けることが多い


## コントローラをテストするべきか
コントローラの場合、多種のテストを行うことになるので勉強になる。初心者の練習用としては最適。
フィーチャースペックはコストが掛かり過ぎるので、薄くするに越したことはない。
特別な理由が無い限りは実装するのが良い。


## コントローラのテストの基本
パターンとしては、getやpostなどのメソッドで適切なパラメータを渡して処理させ、正しい動作が行われていることを確認する。

* 適切な値が埋め込まれているか(assign)
* 適切な出力が行われているか(response)
  * 想定通りのリダイレクトが行われたか
  * 想定通りのビューが出力されたか
* 適切なDB操作が行われたか
* その他
  * メール送信
  * ファイルアップロード
  * ...

また、これら以外にも、公開メソッドを定義している場合はテストするべきだが、コントローラで定義することはないはずなので実質的には行うことはない。


### 単純なスペック
以下はリダイレクトが行われているかの単純なスペック。

    # 保存が完了したらホームページにリダイレクトすること
    it "redirects to the home page upon save" do
      post :create, contact: FactoryGirl.attributes_for(:contact)
      expect(response).to redirect_to root_url
    end


### showメソッドのスペック
詳細表示で使われているメソッドのスペック。
コントローラに適切な値がassignされているか、適切なテンプレートを使った出力がされているかをチェック。

    describe 'GET #show' do

      # @contactに要求された連絡先を割り当てること
      it "assigns the requested contact to @contact" do
        contact = FactroyGirl.create(:contact)
        get :show, id: contact
        expect(assigns(:contact)).to eq contact
      end

      # :showテンプレートを表示すること
      it "renders the :show template" do
        contact = FactroyGirl.create(:contact)
        get :show, id: contact
        expect(response).to render_template :show
      end
    end


### indexメソッドのスペック
一覧表示に使われているメソッドのスペック。
contextでパラメータによる状態の違いをグルーピングしている。

beforeやletを使えば重複コードを無くすことができるが、それらは後ほど説明する。

    describe 'GET #index' do

      # params[:letter]がある場合
      context 'with params[:letter]' do

        # 指定された文字で始まる連絡先を配列にまとめること
        it "populates an array of contacts starting with the letter" do
          smith = FactoryGirl.create(:contact, lastname: 'Smith')
          jones = FactoryGirl.create(:contact, lastname: 'Jones')
          get :index, letter: 'S'
          expect(assigns(:contacts)).to match_array([smith])
        end

        # :indexテンプレートを表示すること
        it "renders the :index view" do
          get :index, letter: 'S'
          expect(response).to render_template :index
        end
      end

      # params[:letter]がない場合
      context 'without params[:letter]' do

        # 全ての連絡先を配列にまとめること
        it "populates an array of all contacts" do
          smith = FactoryGirl.create(:contact, lastname: 'Smith')
          jones = FactoryGirl.create(:contact, lastname: 'Jones')
          get :index
          expect(assigns(:contacts)).to match_array([smith, jones])
        end

        # :indexテンプレートを表示すること
        it "renders the :index view" do
          get :index
          expect(response).to render_template :index
        end
      end
    end

### new/editメソッドのスペック  
登録・編集入力ページのスペック。

    describe 'GET #new' do
      # @contactに新しい連絡先を割り当てること
      it "assigns a new Contact to @contact" do
        get :new
        expect(assigns(:contact)).to be_a_new(Contact)
      end

      # :new テンプレートを表示すること 
      it "renders the :new template" do
        get :new
        expect(response).to render_template :new
      end
    end

    describe 'GET #edit' do
      # @contactに要求された連絡先を割り当てること
      it "assigns the requested contact to @contact" do
        contact = FactoryGirl.create(:contact)
        get :edit, id: contact
        expect(assigns(:contact)).to eq contact
      end

      # :editテンプレートを表示すること
      it "renders the :edit template" do
        contact = FactoryGirl.create(:contact)
        get :edit, id: contact
        expect(response).to render_template :edit
      end
    end


#### be_a_new
*be_a_new* は対象が指定したクラスのインスタンスかつ未保存のレコードであることを検証するマッチャである。

##### TODO ActiveModelを使った場合はbe_a_newが使えないのでそれに関する記述が必要


### createメソッドのスペック
登録入力フォームから値を受け取って登録処理を行うメソッドのスペック。
beforeフックを使って電話番号を作っている。

注意が必要なのはchangeマッチャ。expectで渡されたブロックの実行前後の違いを評価するために使用する。

    describe "POST #create" do
      before :each do
        @phones = [
          FactoryGirl.attributes_for(:phone),
          FactoryGirl.attributes_for(:phone),
          FactoryGirl.attributes_for(:phone)
        ]
      end

      # 有効な属性の場合
      context "with valid attributes" do

        # データベースに新しい連絡先を保存すること
        it "saves the new contact in the database" do
          expect{
            post :create, contact: FactoryGirl.attributes_for(:contact,
              phones_attributes: @phones)
          }.to change(Contact, :count).by(1)
        end

        # contacts#showにリダイレクトすること
        it "redirects to contacts#show" do
          post :create, contact: FactoryGirl.attributes_for(:contact,
            phones_attributes: @phones)
          expect(response).to redirect_to contact_path(assigns(:contact))
        end
      end

      # 無効な属性の場合
      context "with invalid attributes" do

        # データベースに新しい連絡先を保存しないこと
        it "does not save the new contact in the database" do
          expect{
            post :create,
              contact: FactoryGirl.attributes_for(:invalid_contact)
          }.to_not change(Contact, :count)
        end

        # :newテンプレートを再表示すること
        it "re-renders the :new template" do
          post :create,
            contact: FactoryGirl.attributes_for(:invalid_contact)
          expect(response).to render_template :new
        end
      end
    end


### updateメソッドのスペック
編集入力フォームから値を受け取って更新を行うメソッドのスペック。

    describe 'PATCH #update' do
      before :each do
        @contact = create(:contact,
          firstname: 'Lawrence', lastname: 'Smith')
      end

      # 有効な属性の場合
      context "valid attributes" do

        # 要求された@contactを取得すること
        it "located the requested @contact" do
          patch :update, id: @contact, contact: attributes_for(:contact)
          expect(assigns(:contact)).to eq(@contact)
        end

        # @contactの属性を変更すること
        it "changes @contact's attributes" do
          patch :update, id: @contact,
            contact: attributes_for(
              :contact,
              firstname: "Larry",
              lastname: "Smith")
          # DBから再取得
          @contact.reload
          expect(@contact.firstname).to eq("Larry")
          expect(@contact.lastname).to eq("Smith")
        end

        # 更新した連絡先のページへリダイレクトすること
        it "redirects to the updated contact" do
          patch :update, id: @contact, contact: attributes_for(:contact)
          expect(response).to redirect_to @contact
        end
      end

      # 無効な属性の場合
      context "with invalid attributes" do

        # 連絡先の属性を変更しないこと
        it "does not change the contact's attributes" do
          patch :update, id: @contact,
            contact: attributes_for(
              :contact,
              firstname: "Larry",
              lastname: nil)
          @contact.reload
          expect(@contact.firstname).to_not eq("Larry")
          expect(@contact.lastname).to eq("Smith")
        end

        # editテンプレートを再表示すること
        it "re-renders the edit template" do
          patch :update, id: @contact, contact: attributes_for(:invalid_contact)
          expect(response).to render_template :edit
        end
      end
    end


### deleteメソッドのスペック  
削除処理を行うメソッドのスペック。

    describe 'DELETE destroy' do
      before :each do
        @contact = create(:contact)
      end

      # 連絡先を削除すること
      it "deletes the contact" do
        expect{
          delete :destroy, id: @contact
        }.to change(Contact,:count).by(-1)
      end

      # contacts#indexにリダイレクトすること
      it "redirects to contacts#index" do
        delete :destroy, id: @contact
        expect(response).to redirect_to contacts_url
      end
    end



## 入れ子になったルーティング
*/contacts/34/phones/22* のように操作対象が階層化している場合、ルーティングは以下のようになる。

    resources :contacts do
      resources :phones
    end

これを *rake routes* で参照すると */contacts/:contact_id/phones/:id* のようになっている。

この場合のコントローラのテストは以下のようになる。

    describe 'GET #show' do

      # 電話番号用の:showテンプレートを表示すること
      it "renders the :show template for the phone" do
        contact = create(:contact)
        phone = create(:phone, contact: contact)
        get :show, id: phone, contact_id: contact.id
        expect(response).to render_template :show
      end

    end

要するにルーティングに必要なパラメータをすべて渡せば良い。


## HTML以外の出力をテストする
HTML以外の形式で出力する場合、ContentTypeの検証や出力結果に適切な値が含まれるかを検証する。
例えば、CSV出力の検証を行う場合、以下の様なスペックが考えられる。

    describe 'CSV output' do

      # CSVファイルを返すこと
      it "returns a CSV file" do
        get :index, format: :csv
        expect(response.headers['Content-Type']).to have_content 'text/csv'
      end

      # 中身を返すこと
      it 'returns content' do
        create(:contact, firstname: 'Aaron', lastname: 'Sumner', email: 'aaron@sample.com') 
        get :index, format: :csv expect(response.body).to have_content 'Aaron Sumner,aaron@sample.com'
      end
    end

詳細なチェックが必要な場合は、処理をモデルに実装させ、モデルでのテストを行う方が良い。

    # カンマ区切りの値を返すこと
    it "returns comma separated values" do
      create(:contact, firstname: 'Aaron', lastname: 'Sumner', email: 'aaron@sample.com')
      expect(Contact.to_csv).to match /Aaron Sumner,aaron@sample.com/
    end

##### TODO PDFの出力をすることもあるはずなのでそれについても追記予定。

## 参考

* [Yes, You Should Write Controller Tests!](http://solnic.eu/2012/02/02/yes-you-should-write-controller-tests.html)
* [csv・execlファイルの出力について](http://railscasts.com/episodes/362-exporting-csv-and-excel)



# モジュールのテスト

##### TODO 追記予定

## コントローラのモジュール

## モデルのモジュール

### ActiveRecord::Baseを継承している必要があるが、実テーブルは不要な場合

### 実テーブルが必要な場合

* [ActiveRecord依存のModuleをrspecでテストする / Oh My Enter!](http://www.ohmyenter.com/?p=150)



## 特定の依存関係を必要としないモジュール


* [RspecによるRailsのモジュールのテスト（暫定案） [俺の備忘録]](http://o.inchiki.jp/obbr/170)


# スペックのクリーンアップ

## ヘルパーマクロ
テストスイート全体で行いたい処理がある場合はマクロの定義を行う。
マクロは *spec/support* ディレクトリ以下に定義する。

    module LoginMacros
      def set_user_session(user)
        session[:user_id] = user.id
      end
    end

利用したい場合は、FactoryGirlと同様に *RSpec.configure* でincludeするか、 *LoginMacros.set_user_session(user)* のようにする。


## Shared examples

複数のサンプルをまとめて定義することができる。

    shared_examples("public access to contacts") do
      describe 'GET #index' do
        it "populates an array of contacts" do
          get :index
          expect(assigns(:contacts)).to match_array [@contact]
        end

        it "renders the :index view" do
          get :index
          expect(response).to render_template :index
        end
      end

      describe 'GET #show' do
        it "assigns the requested contact to @contact" do
          get :show, id: @contact
          expect(assigns(:contact)).to eq @contact
        end

        it "renders the :show template" do
          get :show, id: @contact
          expect(response).to render_template :show
        end
      end
    end


呼び出しは以下のようにする。

    describe "admin access" do
      before :each do
        set_user_session(create(:admin))
      end

      it_behaves_like "public access to contacts"
    end


## カスタムマッチャ
eqやmatchなど既存のマッチャ以外のものが必要になった場合、カスタムマッチャにて定義する。

例えば、ログインされていない場合にログインフォームにリダイレクトするような処理をテストするためのマッチャは以下のように定義できる。

    RSpec::Matchers.define :require_login do |expected|

      # 検証内容を定義
      match do |actual|
        expect(actual).to redirect_to Rails.application.routes.url_helpers.login_path
      end

      # trueを期待している時の定義 [expect(:foo).to]
      failure_message_for_should do |actual|
        "expected to require login to access the method"
      end

      # falseを期待している時の定義 [expect(:foo).to_not]
      failure_message_for_should_not do |actual|
        "expected not to require login to access the method"
      end

      # 成功時のメッセージ
      description do
        "redirect to the login form"
      end
    end

利用する場合は通常のマッチャと同様。

    it "requires login" do
      get :new
      expect(response).to require_login
    end

* [カスタムマッチャの例](https://www.relishapp.com/rspec/rspec-expectations/v/2-3/docs/custom-matchers)



# 総合テスト
この本ではCucumberもTurnipも使わずにCapybaraのDSLでの実装を行っている。
##### TODO 実際のプロジェクトではどうするか未定

## Capybaraとは
スペックでWEBアプリケーションへのユーザ操作をシミュレートするためのライブラリ。

## CapybaraのDSL
Capybaraのフィーチャーファイルは以下の様な構造になる。

    require 'spec_helper'

    feature 'my feature' do
      background do
        # セットアップの詳細を追加する
      end

      scenario 'my first test' do
        # exampleを書く！
      end

      scenario 'my second test' do
        # exampleを書く！
      end
    end

* featureは入れ子にできない
* featureはひとつのファイルに複数定義できる
* featureの中に複数のscenarioを定義できる


##### TODO CucumberやTurnipを使う場合はGherkinベースになる


## 要素の検索
要素の検索はプレーンテキスト、CSS、XPATHが使える。
要素が複数あるなど、曖昧な一致を行うとエラーになる。


## featureの基本
新しいユーザを登録するシナリオは以下のようになる。

    feature 'User management' do

      # 新しいユーザを追加する
      scenario "adds a new user" do

        # 管理者でログイン
        admin = FactoryGirl.create(:admin)
        sign_in admin

        visit root_path
        expect{
          click_link 'Users'
          click_link 'New User'
          fill_in 'Email', with: 'newuser@example.com'
          find('#password').fill_in 'Password', with: 'secret123'
          find('#password_confirmation').fill_in 'Password confirmation',
            with: 'secret123'
          click_button 'Create User'
        }.to change(User, :count).by(1)

        # ユーザ一覧で登録完了メッセージが表示されていることを確認
        expect(current_path).to eq users_path
        expect(page).to have_content 'New user created'
        within 'h1' do
          expect(page).to have_content 'Users'
        end

        # 対象が登録されていることの確認
        expect(page).to have_content 'newuser@example.com'
      end
    end


## Capybaraにおけるデバッグ
save_and_open_page で現在の状況をHTMLで出力できる。

##### TODO ヘルパーによる自動出力について追記予定


## CapybaraのドライバとJavaScriptのサポート
Capybaraにはいくつかのドライバが切り替えられる。

* RackTest
  標準ドライバ。早いけどJavaScriptは使えない
* Selenium
  Firefox経由でテストするのでJavaScriptがサポートされるが、テスト中にFirefoxを間違って操作すると正しいテストが行えない
* capybara-webkit
* Poltergeist

通常は、標準ドライバとJavaScriptドライバをシナリオごとに切り替えて利用する。

    feature 'Usermanagement' do
      scenario "adds a new user", js: true do
        ...
      end
    end


## データベースのクリア
正しくテストが行われるようにデータベースの初期化を行う必要がある。
##### TODO マスターデータの取り込みやtruncation/trunsactionの違いなどもまとめる

    #spec/spec_helper.rb
    RSpec.configure do |config|

      # これより前の設定は省略...

      # config.use_transactional_fixturesはfalseに設定する
      config.use_transactional_fixtures = false

      config.before(:suite) do
        DatabaseCleaner.strategy = :truncation
      end

      config.before(:each) do
        DatabaseCleaner.start
      end

      config.after(:each) do
        DatabaseCleaner.clean
      end
    end

##### TODO また、Seleniumを使う場合、ActiveRecordに少し細工をする必要があるらしい。要調査

    #spec/support/shared_db_connection.rb
    class ActiveRecord::Base
      mattr_accessor :shared_connection
      @@shared_connection = nil

      def self.connection
        @@shared_connection || retrieve_connection
      end
    end
    
    ActiveRecord::Base.shared_connection = ActiveRecord::Base.connection 

* [Configuring database_cleaner with Rails, RSpec, Capybara, and Selenium | Virtuous Code](http://devblog.avdi.org/2012/08/31/configuring-database_cleaner-with-rails-rspec-capybara-and-selenium/)


# 高速化

## let
各サンプルで共通のデータが必要な場合、beforeよりもlet()を使ったほうがパフォーマンスが向上します。

##### TODO 使ったことないので要調査

    require 'spec_helper'
    
    describe ContactsController do
      let(:contact) do
        create(:contact, firstname: 'Lawrence', lastname: 'Smith')
      end

      describe 'GET #show' do
        # contactに要求された連絡先を割り当てること
        it "assigns the requested contact to contact" do
          get :show, id: contact
          expect(:contact).to eq contact
        end
      end

    end

#### letの特徴
* let()は値をキャッシュする。
* let()は遅延評価される。
* let!()を使うと、exampleを実行する前にcontactが強制的に割り当てられる。


## subject{}とit{]とspecify{}
*it{}* と *specify{}* はシノニム(同義語)である。
*subject{}* はテスト対象のオブジェクトを宣言できる。

    subject { build(:user, firstname: 'John', lastname: 'Doe') }
    it 'returns a full name' do
      should be_named 'John Doe'
    end

以下のように書くことも可能

    subject { build(:user, firstname: 'John', lastname: 'Doe') }
    it { should be_named 'John Doe' } 

##### TODO この辺りはもう少しサンプルを多くしたい


## Shoulda
Shouldaはヘルパーを集めた拡張ライブラリ。

    # 値が空でないか
    subject{ Contact.new } specify { should validates_presence_of :firstname }

yardocでドキュメントを見るのが良いが、以下の記事も参考になる。

* [RSpecがさらに捗る shoulda-matchers のマッチャ 一覧 - 酒と泪とRubyとRailsと](http://morizyun.github.io/blog/shoulda-matchers-rspec-matcher/)


## モックとスタブ
モックは、本物のオブジェクトのフリをするオブジェクトである。
テストダブル(test doubles)と呼ばれることもある。
FactoryGirlで行うことに近いが、データベースにアクセスしないので早い。

スタブは、オブジェクトのメソッドをオーバーライドし、事前に決められた値を返す。
つまり、呼ば出されるとテスト用に本物の結果を返すダミーメソッド。
データベースやネットワークをよく使う処理に利用されることが多い。


モックを作る場合、FactoryGirlのbuild_stubbed()を使う。
このオブジェクトはモデルに備わった各メソッドが使用できるがデータベースには存在しない。


スタブを作る場合、次のようなコードを書くと良い。

    allow(Contact).to receive(:order).with('lastname, firstname').and_return([@contact])

これは Contactモデルのorderのスコープをオーバーライドしている。
渡しているのはorderをする文字列(この場合はlastnameとfirstname)。
返却して欲しい値の指定をしている(@contactは予め作られている)。


### モックとスタブの使用例

    describe 'GET #show' do
      let(:contact) { FactoryGirl.build_stubbed(:contact, firstname: 'Lawrence', lastname: 'Smith') }

      before :each do
        allow(contact).to receive(:persisted?).and_return(true)
        allow(Contact).to receive(:order).with('lastname, firstname').and_return([contact])
        allow(Contact).to receive(:find).with(contact.id.to_s).and_return(contact)
        allow(contact).to receive(:save).and_return(true)

        get :show, id: contact
      end

      it "assigns the requested contact to @contact" do
        expect(assigns(:contact)).to eq contact
      end

      it "renders the :show template" do
        expect(response).to render_template :show
      end
    end


### モックとスタブを使う場面
モックとスタブを使うとモデル内の処理から分離される。
これには利点と欠点がある。

#### モックとスタブの利点
* DB操作などが行われないので早い
* コントローラのテストに余計なもの(モデルの処理)が入りこなまい

#### モックとスタブの欠点
* テストコードが煩雑になりやすい
* 余計なトラブルに繋がりやすい

基本的には使う必要は無い。
テストケースが増えて効率化したい場合に検討する。


## GuardとSporkとSpring
Guardはファイルを監視して関連するテストを自動的に実行する。
監視するファイルのルールはGuardfileに記述。

    # Guard起動後にリターンキーを押すとテスト実行
    all_on_pass: false

*bundle exec guard* で実行する。

Guardは変更を監視してトリガーにするものなので、他にもSassのコンパイルなどもできる。

* [#264 Guard - RailsCasts](http://railscasts.com/episodes/264-guard)


SporkはRailsを待機させて使いまわすことで起動時の待ち時間を省略できる。
類似としてZeusやCommands、Springがある。

##### TODO 今のところspringを採用予定


## タグ
特定のタグをつけたスペックのみを実行することも可能。

    # クレジットカードを処理すること
    it "processes a credit card", focus: true do
      # exampleの詳細
    end

focusタグのみを実行。

    $ bundle exec rspec . --tag focus


RSpec.configureで設定することも可能。

    RSpec.configure do |c|
      c.filter_run focus: true
      c.filter_run_excluding slow: true
    end

こうしておけば、Guardで実行するたびに必要なスペックのみを実行させることができる。


## テストケースの削除
必要ないならば積極的に削除してもよいが、削除しても問題ないか判断しづらい場合は保留にすることを推奨する。
コメントアウトによる削除は可読性を下げるので禁止。

    it "loads a lot of data" do
      pending "no longer necessary"
      # スペックのコードが続く。ただし実行はされない
    end


# その他のテスト

## メール送信をテストする

##### TODO 使ったことないので要調査
[Email Spec](https://github.com/bmabey/email-spec)を使用する

## ファイルアップロードをテストする
ファクトリからファイルにアクセスするならば、 **spec/factories**に置いて以下のように記述すると良い。

    FactoryGirl.define do
      factory :user do
        sequence(:username) { |n| "user#{n}"}
        password 'secret'
        avatar { File.new("#{Rails.root}/spec/factories/avatar.png") }
      end
    end

また、スペックからアクセスするならば以下のようにする。

    # 新しいユーザーを作成できること
    it "creates a new user" do
      visit new_user_url
      fill_in 'Username', with: 'aaron'
      fill_in 'Password', with: 'secret'
      attach_file 'Avatar', File.new("#{Rails.root}/spec/factories/avatar.png")
      click_button 'Sign up'
      expect(User.last.avatar_file_name).to eq 'avatar.png'
    end

これを使ってコントローラのテストを行う場合は以下のようになる。

    it "uploads an avatar" do
      post :create, user: create(:user)
      expect(assigns(:user).avatar_file_name).to eq 'avatar.png'
    end


##### TODO PaperClipなどを使う場合は専用にヘルパーが用意されているはず


## 時間をテストする
特定の日時の動作をテストするには[Timecop](https://github.com/travisjeffery/timecop)を使う。

    # 1月1日に「明けましておめでとう」を訪問者に伝えること
    it "wishes the visitor a Happy New Year on January 1" do
      Timecop.travel Time.parse("January 1")
      visit root_url
      expect(page).to have_content "Happy New Year!"
      Timecop.return
    end

また、 **Timecop.freeze** を使えば現在時刻で止めることができる。
これは、タイムスタンプを検証する際などに使える。

    # 現在時刻でモデルの作成日時が記録されること
    it "stamps the model's created at with the current time" do
      Timecop.freeze
      user = create(:user)
      expect(user.created_at).to eq Time.now
      Timecop.return
    end

## 外部連携をテストする
ケースバイケースだが、基本的にはスタブ化するか、ダミーのリクエストを送るようにする方が良い。

##### TODO 理由とかをもう少しまとめよう


## rake タスクをテストする
rakeタスク内の処理をクラスに追い出して、そのクラスに対してテストする。


##### TODO 具体例を追加する


# テスト駆動開発

## 基本手順

1.テストを書く
  1.フィーチャースペックを作ってアウトラインを定義
  2.シナリオにステップを定義
2.テストをパスさせるコードを書く
3.コードをリファクタリングする

##### TODO コントローラ・モデルのスペックを書くのはどの段階？要調査。おそらくアウトライン定義後


## 短いスパイクを書くのはOK
テストがないとコードを書いてはいけないわけではない。
スパイク(アーキテクチャを確立させるためのプロトタイプ)を作るのは問題ない。
別アプリケーション・またはフィーチャーブランチでスパイクを作成し、
本流に戻ってからテストを書いた後に移植・リファクタリングするのは良い。


## 小さくコードを書き、小さくテストするのもOK
まだスペックから書き始めるのが難しければ、コードを書いてからテストを書いても良い。
その代わり、必ずテストとセットで書くこと。


## フィーチャースペックから書き始めるのが良い
以下の順に書くのが望ましい。

1.フィーチャー
2.コントローラ
3.モデル

フィーチャースペックを定義していけば他のレベルでテストしたほうが良い機能が見つかる。
フィーチャーが関連するテストのアウトラインを導き出してくれる。


# 参考
* [RSpec.info](http://rspec.info/)
  : RSpec本家
* [RelishのRSpecドキュメント](https://www.relishapp.com/rspec)
* [Better Specs](http://betterspecs.org/)
  : ベストプラクティスまとめ

